﻿using AutoMapper;
using BookstoreAPI.Models.Domain;
using BookstoreAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookstoreAPI.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Book, BookReadDTO>()
                .ForMember(bdto=> bdto.Publisher, opt=> opt
                .MapFrom(b=>b.PublisherId))
                .ForMember(bdto => bdto.Authors, opt=>opt
                .MapFrom(b=>b.Authors
                .Select(b=>b.Id)
                .ToArray()))
                .ReverseMap();

            CreateMap<BookCreateDTO, Book>().ForMember(b=>b.PublisherId, opt=>opt.MapFrom(bdto=>bdto.Publisher))
                .ForMember(b=>b.Id, opt=>opt.Ignore())
                .ForMember(b=>b.Publisher, opt=>opt.Ignore())
                .ForMember(b=>b.Authors, opt=>opt.Ignore());
            CreateMap<BookUpdateDTO, Book>().ForMember(b => b.PublisherId, opt => opt.MapFrom(bdto => bdto.Publisher))
                .ForMember(b=>b.Id, opt=>opt.Ignore())
                .ForMember(b => b.Publisher, opt => opt.Ignore())
                .ForMember(b => b.Authors, opt => opt.Ignore());
        }
    }
}
