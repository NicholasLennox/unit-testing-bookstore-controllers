﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookstoreAPI.Data;
using BookstoreAPI.Models.Domain;
using BookstoreAPI.Services;
using BookstoreAPI.Models.DTO;
using AutoMapper;

namespace BookstoreAPI.Controllers
{
    [Route("api/v1/books")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookService _repository;
        private readonly IMapper _mapper;

        public BooksController(IBookService repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET: api/Books
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookReadDTO>>> GetBooks()
        {
            return _mapper.Map<List<BookReadDTO>>(await _repository.GetAllBooksAsync());
        }

        // GET: api/Books/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BookReadDTO>> GetBook(int id)
        {
            var book = await _repository.GetSpecificBookAsync(id);

            if (book == null)
            {
                return NotFound();
            }

            return _mapper.Map<BookReadDTO>(book);
        }

        // PUT: api/Books/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBook(int id, BookUpdateDTO book)
        {
            if (id != book.Id)
            {
                return BadRequest();
            }

            if (!await _repository.BookExistsAsync(id))
            {
                return NotFound();
            }

            try
            {
                await _repository.UpdateBookAsync(_mapper.Map<Book>(book));
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        // POST: api/Books
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BookReadDTO>> PostBook(BookCreateDTO book)
        {
            var newBook = await _repository.AddNewBookAsync(_mapper.Map<Book>(book));

            return CreatedAtAction("GetBook", new { id = newBook.Id }, _mapper.Map<BookReadDTO>(newBook));
        }

        // DELETE: api/Books/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBook(int id)
        {
            if (!await _repository.BookExistsAsync(id))
            {
                return NotFound();
            }

            await _repository.DeleteBookAsync(id);

            return NoContent();
        }
    }
}
