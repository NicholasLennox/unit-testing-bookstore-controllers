﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookstoreAPI.Models.DTO
{
    public class BookReadDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
        public string Genre { get; set; }
        public string Description { get; set; }
        public DateTime PublishedAt { get; set; }
        public int Publisher { get; set; }
        // Refer to linked authors as an integer list of id's for brevity.
        public List<int> Authors { get; set; }
    }
}
