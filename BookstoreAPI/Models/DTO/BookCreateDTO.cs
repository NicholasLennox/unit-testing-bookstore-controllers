﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookstoreAPI.Models.DTO
{
    public class BookCreateDTO
    {
        public string Title { get; set; }
        public string ISBN { get; set; }
        public string Genre { get; set; }
        public string Description { get; set; }
        public DateTime PublishedAt { get; set; }
        public int Publisher { get; set; }
        public List<int> Authors { get; set; }
    }
}
