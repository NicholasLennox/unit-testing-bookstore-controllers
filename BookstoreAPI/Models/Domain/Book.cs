﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BookstoreAPI.Models.Domain
{
    [Table("Book")]
    public class Book
    {
        // Fields
        public int Id { get; set; }
        [MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(13)]
        public string ISBN { get; set; }
        public string Genre { get; set; }
        public string Description { get; set; }
        public DateTime PublishedAt { get; set; }
        public int PublisherId { get; set; }
        // Navigation
        public Publisher Publisher { get; set; }
        public ICollection<Author> Authors { get; set; }
    }
}
