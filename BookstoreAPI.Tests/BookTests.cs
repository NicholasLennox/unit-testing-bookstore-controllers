using AutoMapper;
using BookstoreAPI.Controllers;
using BookstoreAPI.Models.Domain;
using BookstoreAPI.Models.DTO;
using BookstoreAPI.Profiles;
using BookstoreAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Hosting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace BookstoreAPI.Tests
{
    public class BookTests
    {
        private static IMapper _mapper;

        public BookTests()
        {
            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new BookProfile());
                });
                IMapper mapper = mappingConfig.CreateMapper();
                _mapper = mapper;
                mappingConfig.AssertConfigurationIsValid();
            }
        }

        [Fact]
        public async Task GetBooks_ValidBooks_ShouldReturnListOfBookDTOsAsync()
        {
            // Arrange

            // Setup data
            IEnumerable<Book> books = new List<Book>()
            {
                TestUtils.BookFakes.TestBook()
            };
            IEnumerable<BookReadDTO> booksDTO = new List<BookReadDTO>()
            {
                TestUtils.BookFakes.TestBookDTO()
            };
            // Mock repository and create controller
            var mockRepo = new Mock<IBookService>();
            mockRepo.Setup(repo => repo.GetAllBooksAsync()).ReturnsAsync(books);
            var controller = new BooksController(mockRepo.Object, _mapper);
            // Act
            var result = await controller.GetBooks();
            var expeced = JsonSerializer.Serialize(booksDTO);
            var actual = JsonSerializer.Serialize(result.Value);
            // Assert
            Assert.Equal(expeced, actual);
        }

        [Fact]
        public async Task GetBook_InvalidId_ShouldReturn404Async()
        {
            // Arrange
            int id = 2;
            Book returnBook = null;
            var mockRepo = new Mock<IBookService>();
            mockRepo.Setup(repo => repo.GetSpecificBookAsync(id)).ReturnsAsync(returnBook);
            var controller = new BooksController(mockRepo.Object, _mapper);
            int expected = (int)HttpStatusCode.NotFound;
            // Act
            var result = await controller.GetBook(id);
            var objectResult = result.Result as IStatusCodeActionResult;
            int actual = (int)objectResult.StatusCode;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task GetBook_ValidId_ShouldReturnBookDTO()
        {
            // Arrange
            int id = 1;
            Book returnBook = TestUtils.BookFakes.TestBook();
            var mockRepo = new Mock<IBookService>();
            mockRepo.Setup(repo => repo.GetSpecificBookAsync(id)).ReturnsAsync(returnBook);
            var controller = new BooksController(mockRepo.Object, _mapper);
            var expected = JsonSerializer.Serialize(TestUtils.BookFakes.TestBookDTO());
            // Act
            var result = await controller.GetBook(id);
            var actual = JsonSerializer.Serialize(result.Value);
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
