﻿using BookstoreAPI.Models.Domain;
using BookstoreAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreAPI.Tests.TestUtils
{
    public class BookFakes
    {
        public static Book TestBook()
        {
            Book testBook = new Book()
            {
                Id = 1,
                ISBN = "978-3-16-148410-0",
                Title = "Test Book",
                Description = "Sample description",
                Genre = "Sample genre",
                PublisherId = 1,
                PublishedAt = DateTime.MinValue,
                Authors = TestAuthors()
            };
            return testBook;
        }

        public static BookReadDTO TestBookDTO()
        {
            BookReadDTO testBook = new BookReadDTO()
            {
                Id = 1,
                ISBN = "978-3-16-148410-0",
                Title = "Test Book",
                Description = "Sample description",
                Genre = "Sample genre",
                Publisher = 1,
                PublishedAt = DateTime.MinValue,
                Authors = new List<int>{ 1 }
            };
            return testBook;
        }

        private static List<Author> TestAuthors()
        {
            Author testAuthor = new Author()
            {
                Id = 1,
                Name = "Test Author"
            };
            return new List<Author>() { testAuthor };
        }
    }
}
